// cadrado1 ex20191010f

// cadrado de *, con dimensiones entre [2, 20]

#include <stdio.h>

int main () {
    int width;
    int height;

    do {
        printf("Width [2-20]: ");
        scanf("%d", &width);
    } while (width < 2 || width > 20);
    
    do {
        printf("Height [2-20]: ");
        scanf("%d", &height);
    } while (height < 2 || height > 20);

    printf("\n");
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            printf(" *");
        }
        printf("\n");
    }
    printf("\n");
    
    return 0;
}