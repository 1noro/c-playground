// calculoImporte ex20190926a

// Estructura selectiva simple
// Solicitanse unidades e prezo dun produto e presentanse.
// Se se supera o valor de 100E calcularase un desconto do 5% sobre o total.

#include <stdio.h>

static int P_DESCUENTO = 5;

int main() {
    float precio;
    int unidades;
    float total;
    printf("Introduce el precio del producto: ");
    scanf("%f", &precio);
    printf("Introduce el número de unidades: ");
    scanf("%d", &unidades);
    total = precio * (float) unidades;
    printf("El precio total es: %.2f€\n", total);
    if (total > 100) {
        float a_descontar = total * ((float) P_DESCUENTO) / ((float) 100);
        // printf("%f = %f * (%f)\n", a_descontar, total, ((float) P_DESCUENTO) / ((float) 100));
        float total_con_descuento = total - a_descontar;
        printf("A descontar (%d%%): %.4f€, Total con el descuento: %.2f€\n", P_DESCUENTO, a_descontar, total_con_descuento);
    }
    return 0;
}
