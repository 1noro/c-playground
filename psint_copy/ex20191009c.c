// rangoNumeros ex20191009c

// mientras
// definir un rango [n1, n2]
// pedir números enteros dentro de ese rango
// exit - fuera de rango
// contar numeros y sumar los del bucle

#include <stdio.h>

int main () {
    int input;
    int n1;
    int n2;
    int suma = 0;
    int contador = 0;

    printf("Define el primer número del rango [n1, n2]: ");
    scanf("%d", &n1);

    printf("Define el segundo número del rango [%d, n2]: ", n1);
    scanf("%d", &n2);

    printf("Escribe un número (fuera de rango -> exit): ");
    scanf("%d", &input);

    while (input >= n1 && input <= n2) {
        suma += input;
        contador++;
        printf("Escribe un número (fuera de rango -> exit): ");
        scanf("%d", &input);
    }

    printf("Has introducido %d números y, en total, suman %d.\n", contador, suma);

    return 0;
}