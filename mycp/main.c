
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// opendir
#include <dirent.h>

// to check if a path is a file or a directory
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


const int BUFF_SIZE = 8192; // 8192 bytes ¿512 bytes?


int is_regular_file(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) != 0) {
        printf("Error: stat over '%s'\n", path);
        return 0;
    }
    return S_ISREG(path_stat.st_mode);
}


int is_directory(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) != 0) {
        printf("Error: stat over '%s'\n", path);
        return 0;
    }
    return S_ISDIR(path_stat.st_mode);
}


int copy_file(const char *of_path, const char *df_path) {
    FILE *of_ptr; // origin_file_ptr
    FILE *df_ptr; // destination_file_ptr

    of_ptr = fopen(of_path, "r");
    if (of_ptr == NULL) {
        // printf("Error: File '%s' is not available\n", of_path);
        return 1;
    } else {
        df_ptr = fopen(df_path, "w");
        if (df_ptr == NULL) {
            // printf("Error: File '%s' is not available\n", df_path);
            return 1;
        } else {
            size_t elem_read; // elements read
            size_t elem_writed; // elements writed
            unsigned char buff[BUFF_SIZE];

            do {
                elem_read = fread(buff, 1, sizeof(buff), of_ptr);
                if (elem_read > 0) {
                    elem_writed = fwrite(buff, 1, elem_read, df_ptr);
                } else {
                    elem_writed = 0;
                }
            } while ((elem_read > 0) && (elem_read == elem_writed));
            // if (elem_writed) perror("copy"); // no se que significa, revisar

            fclose(df_ptr);
        }
        fclose(of_ptr);
    }

    return 0;
}


int copy_directory(const char *od_path, const char *dd_path) {
    struct dirent *od_dirent;
    DIR *od_ptr;

    od_ptr = opendir(od_path);
    if (od_ptr == NULL) {
        printf("Error: '%s' could not be oppen\n", od_path);
        return 0;
    } else {
        while ((od_dirent = readdir(od_ptr)) != NULL) {
            printf("%s/%s\n", od_path, od_dirent->d_name);
            if (!strcmp(od_dirent->d_name, ".") && !strcmp(od_dirent->d_name, "..")) {
                char *o_path;
                o_path = (char*) malloc(sizeof(od_path) + sizeof(od_dirent->d_name) + 1 * sizeof(char)); // (+ 1 * sizeof(char)) por la '/'
                strcpy(o_path, od_path);
                strcpy(o_path, "/");
                strcpy(o_path, od_dirent->d_name);

                char *d_path;
                d_path = (char*) malloc(sizeof(dd_path) + sizeof(od_dirent->d_name) + 1 * sizeof(char)); // (+ 1 * sizeof(char)) por la '/'
                strcpy(d_path, dd_path);
                strcpy(d_path, "/");
                strcpy(d_path, od_dirent->d_name);

                if (is_regular_file(o_path)) {
                    printf("- %s -> %s\n", o_path, d_path);
                    /*int err = copy_file(o_path, d_path);
                    if (err != 0) {
                        printf("Error: '%s' could not be copied\n", o_path);
                    }*/
                } else if (is_directory(o_path)) {
                    // COMPROBAR SI EL DIRECTORIO DE ORIGEN ES DIFERENTE AL DIRECTORIO DE DESTINO
                    printf("D %s -> %s\n", o_path, d_path);
                    copy_directory(o_path, d_path);
                } else {
                    printf("Error: '%s' is not a supported file\n", o_path);
                }

                free(o_path);
                free(d_path);
            }
        }
        closedir(od_ptr);
    }

    return 0;
}


int main(int argc, char *argv[]) {
    char *o_path;  // origin_path_ptr
    char *d_path;  // destination_path_ptr

    if (argc < 3) {
        printf("Sintax: mycp <origin> <destination>\n");
        exit(1);
    }

    o_path = argv[1];
    d_path = argv[2];

    if (is_regular_file(o_path)) {
        int err = copy_file(o_path, d_path);
        if (err != 0) {
            printf("Error: '%s' could not be copied\n", o_path);
        }
    } else if (is_directory(o_path)) {
        copy_directory(o_path, d_path);
    } else {
        printf("Error: '%s' is not a supported file\n", o_path);
    }

    return 0;
}
