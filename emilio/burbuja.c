#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void get_char_with_left_zeros(int *num, int left_zeros, char *result) {
    sprintf(result, "%d", *num);
}

/*
void print_pretty_int_array(int *array, int *array_length) {
    printf("[");
    for (int i = 0; i < *array_length; i++) {
        if (i < *array_length - 1) {
            printf("%d, ", array[i]);
        } else {
            printf("%d", array[i]);
        }
    }
    printf("]\n");
}
*/

void print_array(int *array, int *array_length) {
    printf("[");
    for (int i = 0; i < *array_length; i++) {
        if (i < *array_length - 1) {
            printf("%d, ", array[i]);
        } else {
            printf("%d", array[i]);
        }
    }
    printf("]\n");
}


void ordenar_burbuja(int *array, int *array_length) {
    int i1, i2, aux;
    for (i1 = 0; i1 < *array_length - 1; i1++) {
        for (i2 = i1; i2 < *array_length; i2++) {
            if (array[i1] > array[i2]) {
                aux = array[i1];
                array[i1] = array[i2];
                array[i2] = aux;
            }
        }
    }
}


int main() {
    srand(time(NULL)); // Initialization, should only be called once.

    int array_length;
    printf("Escribe el tamaño del array: ");
    scanf("%d", &array_length);

    int array[array_length];

    for (int i = 0; i < array_length; i++) {
        // printf("Escribe el valor en la posición %d: ", i);
        // scanf("%d", &array[i]);
        array[i] = rand() % 500 + 1;
    }

    print_array(array, &array_length);
    ordenar_burbuja(array, &array_length);
    print_array(array, &array_length);

    // pruebas para left zeros
    int num = 1235;
    int left_zeros = 10;
    char result[left_zeros];
    get_char_with_left_zeros(&num, left_zeros, result);
    printf("'%s'\n", result);

    return 0;
}
