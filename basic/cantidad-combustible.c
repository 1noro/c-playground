#include<stdio.h>

int main(int argc, char const *argv[]) {
    int n_turismos, n_todoterrenos, c_turismo, c_todoterreno;

    printf("Escribe el número de turismos: ");
    scanf("%d", &n_turismos);

    printf("Escribe el número de todoterrenos: ");
    scanf("%d", &n_todoterrenos);

    printf("Escribe la capacidad de combustible de un turismo (l): ");
    scanf("%d", &c_turismo);

    printf("Escribe la capacidad de combustible de un todoterreno (l): ");
    scanf("%d", &c_todoterreno);

    int litros_necesarios = n_turismos * c_turismo + n_todoterrenos * c_todoterreno;
    printf("Los litros necesarios son: %d\n", litros_necesarios);

    return 0;
}
